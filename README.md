# DesktopGame-SE575

Build and deploy all server (kafka, mongoDB, CommandServer):

$ bash ./restart_all.sh
    or on Windows:
$ restart_all.bat

All three servers are handled by docker-compose and running on local host.

Build client: open the ./DesktopGameClient/DesktopGameClient.sln in Visual Studio and hit Build!
The release .exe is in ./DesktopGameClient/DesktopGameClient/bin/Release/DesktopGameClient.exe, which only works on localhost servers (sorry for hard coding that!)

