﻿using DesktopGameClient.backend;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace DesktopGameClient {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Remove is not implemented!
    /// </summary>
    public partial class MainWindow : Window {

        readonly ImageMongoDB mongo;
        readonly KafkaClient kafka;
        readonly LocalImage localImage;
        readonly DesktopManager desktop;

        BindingList<ImageFile> imageFileList;

        // the entry point of the Client
        // init all backend classes
        public MainWindow() {
            InitializeComponent();
            string clientID = "USER" + new Random().Next();

            imageFileList = new BindingList<ImageFile>();
            ImageListBox.ItemsSource = imageFileList;

            mongo = new ImageMongoDB();
            localImage = new LocalImage(mongo, clientID);

            UpdateImageList("");

            kafka = new KafkaClient(clientID);
            kafka.InitProducers();
            kafka.InitDialogueConsumer(UpdateDialogueBox);
            kafka.InitImageDBUpdatesConsumer(UpdateImageList);

            desktop = new DesktopManager(localImage, kafka, DesktopCanvas);
        }

        private void UpdateDialogueBox(string dialogue) {
            Dispatcher.Invoke(() => DialogueLogBox.Text += dialogue + "\n");
        }

        protected override void OnClosed(EventArgs e) {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }

        private void UpdateImageList(string dialogue) {
            Console.WriteLine("Refreshing image list...");
            Dispatcher.Invoke(() => {
                imageFileList.Clear();
            });
            Task.Run(() => {
                Task<List<ImageFile>> t1 = mongo.GetImageList();
                Console.WriteLine($"Get {t1.Result.Count} files");
                t1.Result.ForEach(file => {
                    if (!localImage.HasImage(file)) {
                        Task t2 = localImage.DownloadImage(file);
                    }
                    Dispatcher.Invoke(() => {
                        imageFileList.Add(file);
                    });
                    Console.WriteLine($"{imageFileList.Count} files");
                });
            });
        }

        private void DialogueSendBtn_Click(object sender, RoutedEventArgs e) {

            string dialogue = DialogueInputBox.Text.Trim();

            if (dialogue.Length > 0) {
                if (dialogue[0] == '/') {
                    Task t1 = kafka.SendCommand(dialogue.Substring(1));
                }

                if (DialogueSpeakerBox.Text.Length > 0) {
                    dialogue = DialogueSpeakerBox.Text + " : " + dialogue;
                }
                Task t2 = kafka.SendDialogue(dialogue);
                DialogueInputBox.Text = "";
            }
        }

        private void UploadOpenFileBtn_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog {
                Filter = "Image files (*.png;*.jpg)|*.png;*.jpg"
            };
            if (openFileDialog.ShowDialog() == true) {
                Task t = UploadImage(openFileDialog.FileName);
            }
        }

        private void AddImageToCanvasBtn_Click(object sender, RoutedEventArgs e) {
            if (ImageListBox.SelectedItem is ImageFile file) {
                Task t = kafka.SendDesktopUpdate(DesktopUpdateMessage.NewItemMessage(file).ToString());
            }
        }

        private async Task UploadImage(string imagePath) {
            MongoDB.Bson.ObjectId id = await mongo.UploadFile(imagePath);
            await kafka.SendImageDBUpdate($"IMG_UPLOADED {id}");
        }
    }
}
