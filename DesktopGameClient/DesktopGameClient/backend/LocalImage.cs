﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;


namespace DesktopGameClient.backend {
    /// <summary>
    /// Manages local image files
    /// </summary>
    public class LocalImage {
        string localImageDir;
        readonly Dictionary<ObjectId, string> imageDict;
        readonly ImageMongoDB mongo;

        public LocalImage(ImageMongoDB mongo, string clientID) {
            this.mongo = mongo;
            CreateDir(clientID);
            imageDict = new Dictionary<ObjectId, string>();
        }

        private void CreateDir(string clientID) {
            localImageDir = Path.Combine(Path.GetTempPath(), $"DG_{clientID}");
            Directory.CreateDirectory(localImageDir);
        }

        public async Task DownloadImage(ImageFile file) {
            string imagePath = Path.Combine(localImageDir, file.ObjectID.ToString());
            await mongo.DownloadFile(file.ObjectID, imagePath);
            imageDict.Add(file.ObjectID, imagePath);
        }

        public string GetImagePath(ImageFile file) {
            return imageDict[file.ObjectID];
        }

        public bool HasImage(ImageFile file) {
            return imageDict.Keys.Contains(file.ObjectID);
        }

        public Image LoadImage(ImageFile file) {

            BitmapImage src = new BitmapImage();
            src.BeginInit();

            if (!HasImage(file)) {
                Task<Stream> t = mongo.LoadFileToStream(file.ObjectID);
                src.StreamSource = t.Result;
            } else {
                src.UriSource = new Uri(GetImagePath(file));
            }

            src.CacheOption = BitmapCacheOption.OnLoad;
            src.EndInit();

            Image image = new Image {
                Source = src
            };
            return image;
        }
    }
}
