﻿using Confluent.Kafka;
using System;
using System.Threading.Tasks;

namespace DesktopGameClient.backend {
    public delegate void ConsumeMethod(string message);

    public class KafkaClient {
        static readonly string KafkaServer = "localhost:9092";
        static readonly string DialogueTopic = "test-dialogue";
        static readonly string CommandTopic = "test-command";
        static readonly string ImageDBUpdateTopic = "test-image-update";
        static readonly string DesktopTopic = "test-desktop";

        Producer<string, string> DialogueProducer;
        Producer<string, string> CommandProducer;
        Producer<string, string> ImageDBUpdateProducer;
        Producer<string, string> DesktopProducer;

        string clientID;

        public KafkaClient(string id) {
            clientID = id;
        }

        public void InitProducers() {
            var config = new ProducerConfig { BootstrapServers = KafkaServer };
            DialogueProducer = new Producer<string, string>(config);
            CommandProducer = new Producer<string, string>(config);
            ImageDBUpdateProducer = new Producer<string, string>(config);
            DesktopProducer = new Producer<string, string>(config);
        }

        public void InitDialogueConsumer(ConsumeMethod consume) {
            var conf = new ConsumerConfig {
                GroupId = clientID,
                BootstrapServers = KafkaServer,
                AutoOffsetReset = AutoOffsetResetType.Earliest
            };

            Task.Run(() => {
                using (var c = new Consumer<string, string>(conf)) {
                    c.Subscribe(DialogueTopic);
                    bool consuming = true;
                    c.OnError += (_, e) => consuming = !e.IsFatal;

                    while (consuming) {
                        try {
                            var cr = c.Consume();
                            consume(cr.Value);
                            Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.Topic}'.");
                        } catch (ConsumeException e) {
                            Console.WriteLine($"Error occured: {e.Error.Reason}");
                        }
                    }
                    Console.WriteLine($"Consumer Error: {DialogueTopic}.");
                    c.Close();
                }
            });
        }

        public void InitImageDBUpdatesConsumer(ConsumeMethod consume) {
            var conf = new ConsumerConfig {
                GroupId = clientID,
                BootstrapServers = KafkaServer,
                AutoOffsetReset = AutoOffsetResetType.Latest 
            };

            Task.Run(() => {
                using (var c = new Consumer<string, string>(conf)) {
                    c.Subscribe(ImageDBUpdateTopic);
                    bool consuming = true;
                    c.OnError += (_, e) => consuming = !e.IsFatal;

                    while (consuming) {
                        try {
                            var cr = c.Consume();
                            consume(cr.Value);
                            Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.Topic}'.");
                        } catch (ConsumeException e) {
                            Console.WriteLine($"Error occured: {e.Error.Reason}");
                        }
                    }
                    c.Close();
                }
            });
        }

        public void InitDesktopUpdateConsumer(ConsumeMethod consume) {
            var conf = new ConsumerConfig {
                GroupId = clientID,
                BootstrapServers = KafkaServer,
                AutoOffsetReset = AutoOffsetResetType.Earliest
            };

            Task.Run(() => {
                using (var c = new Consumer<string, string>(conf)) {
                    c.Subscribe(DesktopTopic);
                    bool consuming = true;
                    c.OnError += (_, e) => consuming = !e.IsFatal;

                    while (consuming) {
                        try {
                            var cr = c.Consume();
                            consume(cr.Value);
                            Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.Topic}'.");
                        } catch (ConsumeException e) {
                            Console.WriteLine($"Error occured: {e.Error.Reason}");
                        }
                    }
                    c.Close();
                }
            });
        }

        private async Task SendMessage(Producer<string, string> producer, string topic, string dialogue) {
            try {
                var dr = await producer.ProduceAsync(topic,
                    new Message<string, string> {
                        Key = clientID,
                        Value = dialogue
                    });
                Console.WriteLine($"Delivered '{dr.Value}' to '{dr.Topic}'");
            } catch (KafkaException e) {
                Console.WriteLine($"Delivery failed: {e.Error.Reason}");
            }
        }

        public async Task SendDialogue(string dialogue) {
            await SendMessage(DialogueProducer, DialogueTopic, dialogue);
        }

        public async Task SendCommand(string command) {
            await SendMessage(CommandProducer, CommandTopic, command);
        }

        public async Task SendImageDBUpdate(string m) {
            await SendMessage(ImageDBUpdateProducer, ImageDBUpdateTopic, m);
        }

        public async Task SendDesktopUpdate(string m) {
            await SendMessage(DesktopProducer, DesktopTopic, m);
        }
    }

    class KafkaTest {
        public static async Task TestProducer() {
            var config = new ProducerConfig { BootstrapServers = "localhost:9092" };

            // A Producer for sending messages with null keys and UTF-8 encoded values.
            using (var p = new Producer<Null, string>(config)) {
                try {
                    Console.WriteLine("Start");
                    var dr = await p.ProduceAsync("test", new Message<Null, string> { Value = "test" });
                    Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                } catch (KafkaException e) {
                    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                }
            }
        }

        public static void TestConsumer() {
            var conf = new ConsumerConfig {
                GroupId = "test-consumer-group",
                BootstrapServers = "localhost:9092",
                // Note: The AutoOffsetReset property determines the start offset in the event
                // there are not yet any committed offsets for the consumer group for the
                // topic/partitions of interest. By default, offsets are committed
                // automatically, so in this example, consumption will only start from the
                // eariest message in the topic 'my-topic' the first time you run the program.
                AutoOffsetReset = AutoOffsetResetType.Earliest
            };

            using (var c = new Consumer<Ignore, string>(conf)) {
                c.Subscribe("test");

                bool consuming = true;
                // The client will automatically recover from non-fatal errors. You typically
                // don't need to take any action unless an error is marked as fatal.
                c.OnError += (_, e) => consuming = !e.IsFatal;

                while (consuming) {
                    try {
                        var cr = c.Consume();
                        Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
                    } catch (ConsumeException e) {
                        Console.WriteLine($"Error occured: {e.Error.Reason}");
                    }
                }

                // Ensure the consumer leaves the group cleanly and final offsets are committed.
                c.Close();
            }
        }
    }
}
