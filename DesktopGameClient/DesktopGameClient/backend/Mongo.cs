﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopGameClient {

    /// <summary>
    /// Wraps MongoDB interactions
    /// </summary>
    public class ImageMongoDB {
        private static readonly string serverAdd = "mongodb://root:example@127.0.0.1:27017";
        private static readonly string databaseName = "desktop_game_images";
        private static readonly string fileCollectionName = "fs.files";

        private readonly MongoClient client;
        private readonly IMongoCollection<BsonDocument> fileCollection;
        private readonly GridFSBucket fs;

        public ImageMongoDB() {
            client = new MongoClient(serverAdd);
            var database = client.GetDatabase(databaseName);
            fileCollection = database.GetCollection<BsonDocument>(fileCollectionName);
            fs = new GridFSBucket(database);
        }

        public async Task<ObjectId> UploadFile(string path) {
            Console.WriteLine($"Start uploading {path}");
            using (var s = File.OpenRead(path)) {
                ObjectId id = await fs.UploadFromStreamAsync(Path.GetFileName(path), s);
                Console.WriteLine($"Finished uploading {path}");
                return id;
            }
        }

        public async Task<List<ImageFile>> GetImageList() {
            List<BsonDocument> docList = await fileCollection.Find(new BsonDocument()).ToListAsync();
            List<ImageFile> result =
                docList.Select(doc => new ImageFile {
                    FileName = doc["filename"].AsString,
                    ObjectID = doc["_id"].AsObjectId
                })
                    .ToList();
            return result;
        }

        public async Task DownloadFile(ObjectId id, string path) {
            Console.WriteLine($"Start downloading {id} to {path}");
            using (var w = File.OpenWrite(path)) {
                await fs.DownloadToStreamAsync(id, w);
            }
        }

        public async Task<Stream> LoadFileToStream(ObjectId id) {
            MemoryStream stream = new MemoryStream();
            await fs.DownloadToStreamAsync(id, stream);
            return stream;
        }
    }

    public class ImageFile {
        public string FileName { get; set; }
        public ObjectId ObjectID { get; set; }
    }
}
