﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DesktopGameClient.backend {

    /// <summary>
    /// Handles the Canvas Logic based on the DesktopUpdation
    /// </summary>
    public class DesktopManager {
        readonly Dictionary<string, DesktopItem> itemDict;
        readonly Dictionary<Image, DesktopItem> imageDict;
        readonly LocalImage images;
        readonly Canvas desktopCanvas;
        readonly KafkaClient kafka;

        public DesktopManager(LocalImage localImages, KafkaClient kafka, Canvas canvas) {
            itemDict = new Dictionary<string, DesktopItem>();
            imageDict = new Dictionary<Image, DesktopItem>();

            images = localImages;
            desktopCanvas = canvas;
            this.kafka = kafka;

            kafka.InitDesktopUpdateConsumer(Consume);
        }

        public void Consume(string message) {
            DesktopUpdateMessage m = new DesktopUpdateMessage(message);
            switch (m.Type) {
                case DesktopUpdateMessage.MessageType.Add:
                    AddItem(m);
                    break;
                case DesktopUpdateMessage.MessageType.Update:
                    UpdateItem(m);
                    break;
            }
        }

        public DesktopItem GetItemFromImage(Image i) {
            return imageDict[i];
        }

        private void AddItem(DesktopUpdateMessage message) {
            desktopCanvas.Dispatcher.Invoke(() => {
                Image i = images.LoadImage(message.Item.File);

                i.Width = 64;
                i.Height = 64;

                EnableDrag(i);

                message.Item.Image = i;

                itemDict.Add(message.Item.ItemId, message.Item);
                imageDict.Add(i, message.Item);

                desktopCanvas.Children.Add(i);
                Canvas.SetLeft(i, message.Item.X);
                Canvas.SetRight(i, message.Item.Y);
            });
        }

        private void UpdateItem(DesktopUpdateMessage message) {
            Image i = itemDict[message.Item.ItemId].Image;

            desktopCanvas.Dispatcher.Invoke(() => {
                Canvas.SetLeft(i, message.Item.X);
                Canvas.SetTop(i, message.Item.Y);
            });
        }

        private void EnableDrag(Image element) {
            Point? dragStart = null;
            double startLeft = 0;
            double startTop = 0;

            void mouseDown(object sender, MouseButtonEventArgs args) {
                dragStart = args.GetPosition(element);
                element.CaptureMouse();

                startLeft = Canvas.GetLeft(element);
                startTop = Canvas.GetTop(element);
            }

            void mouseUp(object sender, MouseButtonEventArgs args) {
                dragStart = null;
                element.ReleaseMouseCapture();

                DesktopItem i = GetItemFromImage(element);
                i.X = (int) Canvas.GetLeft(element);
                i.Y = (int) Canvas.GetTop(element);

                Canvas.SetLeft(element, startLeft);
                Canvas.SetTop(element, startTop);

                Task t = kafka.SendDesktopUpdate(
                    DesktopUpdateMessage.UpdateItemMessage(GetItemFromImage(element))
                    .ToString());
            }

            void mouseMove(object sender, MouseEventArgs args) {
                if (dragStart != null && args.LeftButton == MouseButtonState.Pressed) {
                    var p2 = args.GetPosition(desktopCanvas);
                    Canvas.SetLeft(element, p2.X - dragStart.Value.X);
                    Canvas.SetTop(element, p2.Y - dragStart.Value.Y);
                }
            }

            element.MouseDown += mouseDown;
            element.MouseMove += mouseMove;
            element.MouseUp += mouseUp;
        }
    }

    class DesktopUpdateMessage {
        public enum MessageType { Add, Update, Remove }

        public MessageType Type { get; set; }
        public DesktopItem Item { get; set; }

        static readonly Random r = new Random();

        DesktopUpdateMessage() { }

        public DesktopUpdateMessage(string message) {
            string[] parts = message.Split(' ');
            Type = (MessageType)Enum.Parse(typeof(MessageType), parts[0]);
            Item = new DesktopItem {
                ItemId = parts[1],
                File = new ImageFile {
                    FileName = parts[2],
                    ObjectID = new MongoDB.Bson.ObjectId(parts[3]),
                },
                X = int.Parse(parts[4]),
                Y = int.Parse(parts[5])
            };
        }

        public static DesktopUpdateMessage NewItemMessage(ImageFile file) {
            DesktopUpdateMessage message = new DesktopUpdateMessage() {
                Type = MessageType.Add,
                Item = new DesktopItem {
                    ItemId = r.Next().ToString(),
                    File = file,
                    X = 200,
                    Y = 200
                }
            };
            return message;
        }

        public static DesktopUpdateMessage UpdateItemMessage(DesktopItem item) {
            DesktopUpdateMessage message = new DesktopUpdateMessage() {
                Type = MessageType.Update,
                Item = item
            };
            return message;
        }

        public override string ToString() {
            return $"{Type.ToString()} {Item.ItemId} {Item.File.FileName} {Item.File.ObjectID} {Item.X} {Item.Y}";
        }
    }

    public class DesktopItem {
        public string ItemId { get; set; }
        public ImageFile File { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public Image Image { get; set; }
    }
}
