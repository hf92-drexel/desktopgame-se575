﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CommandServer {

    /// <summary>
    /// A server which handles command messages
    /// </summary>
    class CommandProcessor {
        static void Main(string[] args) {
            Console.WriteLine("Start running...");
            new Thread(new ThreadStart(InitConsumer)).Start();
        }
        private static readonly string commandServerGroupID = "COMMAND_SERVER";
        private static readonly string KafkaServer = "127.0.0.1:9092";
        private static readonly string DialogueTopic = "test-dialogue";
        private static readonly string CommandTopic = "test-command";
        private static readonly string TestTopic = "test-command";

        private static Producer<string, string> producer;
        private static readonly Random r = new Random();

        private readonly string clientID;

        public CommandProcessor(string clientID) {
            this.clientID = clientID;
        }

        static void InitConsumer() {
            InitProducer();
            Console.WriteLine("Sending test message...");
            Task test = SendMessage(TestTopic, "test", "command server is now online!");

            var conf = new ConsumerConfig {
                GroupId = commandServerGroupID,
                BootstrapServers = KafkaServer,
                AutoOffsetReset = AutoOffsetResetType.Latest
            };

            using (var c = new Consumer<string, string>(conf)) {
                c.Subscribe(CommandTopic);

                bool consuming = true;
                c.OnError += (_, e) => consuming = !e.IsFatal;

                while (consuming) {
                    try {
                        var cr = c.Consume();
                        Task t = new CommandProcessor(cr.Key).EvalCommand(cr.Value);
                        Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
                    } catch (ConsumeException e) {
                        Console.WriteLine($"Error occured: {e.Error.Reason}");
                    }
                }
                c.Close();
            }
        }

        async Task EvalCommand(string command) {
            int firstSpace = command.Trim().IndexOf(' ');
            string commandName = command.Substring(0, firstSpace < 0 ? 0 : firstSpace);

            if (commandName.Length == 0) {
                await SendMessage(DialogueTopic, "Command Error: Invalid Command");
            } else {
                string subCommand = firstSpace + 1 >= command.Length ? "" : command.Substring(firstSpace + 1);
                switch (commandName) {
                    case "r":
                    case "roll":
                        EvalRNDCommand(subCommand);
                        break;
                    default:
                        Task t = SendMessage(DialogueTopic, "Command Error: No such command");
                        break;
                }
            }
        }

        void EvalRNDCommand(string command) {
            int firstSpace = command.Trim().IndexOf(' ');
            string rndExp = command.Substring(0, firstSpace < 0 ? command.Length : firstSpace);
            string descption = firstSpace < command.Length && firstSpace > 0 ? command.Substring(firstSpace + 1) : "";

            int result = EvalRND(rndExp);
            if (result > 0) {
                Task t = SendMessage(DialogueTopic, $"Dice: {rndExp}={result} {descption}");
            }
        }

        int EvalRND(string rndExp) {
            string[] parts = rndExp.Split('d');

            try {
                switch (parts.Length) {
                    case 1:
                        int num = short.Parse(parts[0]);
                        return num;
                    case 2:
                        int diceCount = short.Parse(parts[0]);
                        int diceFaces = short.Parse(parts[1]);
                        IEnumerable<int> result = from value in Enumerable.Range(0, diceCount)
                                                  select r.Next(1, diceFaces);
                        return result.Sum();
                    default:
                        throw new FormatException();
                }
            } catch (Exception) {
                Task t = SendMessage(DialogueTopic, "Dice Error: Invalid Dice Settings");
                return -1;
            }
        }

        static void InitProducer() {
            var config = new ProducerConfig { BootstrapServers = KafkaServer };
            producer = new Producer<string, string>(config);
        }

        static async Task SendMessage(string topic, string key, string dialogue) {
            try {
                var dr = await producer.ProduceAsync(topic,
                    new Message<string, string> {
                        Key = key,
                        Value = dialogue
                    });
                Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
            } catch (KafkaException e) {
                Console.WriteLine($"Delivery failed: {e.Error.Reason}");
            }
        }

        async Task SendMessage(string topic, string dialogue) {
            await SendMessage(topic, clientID, dialogue);
        }
    }
}
