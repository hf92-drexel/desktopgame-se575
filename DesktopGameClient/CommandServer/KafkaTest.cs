﻿using Confluent.Kafka;
using System;
using System.Threading.Tasks;

namespace CommandServer {
    class KafkaTest {
        public static async Task TestProducer() {
            var config = new ProducerConfig { BootstrapServers = "localhost:9092" };

            // A Producer for sending messages with null keys and UTF-8 encoded values.
            using (var p = new Producer<Null, string>(config)) {
                try {
                    Console.WriteLine("Start");
                    var dr = await p.ProduceAsync("test", new Message<Null, string> { Value = "test" });
                    Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                } catch (KafkaException e) {
                    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                }
            }
        }

        public static void TestConsumer() {
            var conf = new ConsumerConfig {
                GroupId = "test-consumer-group",
                BootstrapServers = "localhost:9092",
                // Note: The AutoOffsetReset property determines the start offset in the event
                // there are not yet any committed offsets for the consumer group for the
                // topic/partitions of interest. By default, offsets are committed
                // automatically, so in this example, consumption will only start from the
                // eariest message in the topic 'my-topic' the first time you run the program.
                AutoOffsetReset = AutoOffsetResetType.Earliest
            };

            using (var c = new Consumer<Ignore, string>(conf)) {
                c.Subscribe("test");

                bool consuming = true;
                // The client will automatically recover from non-fatal errors. You typically
                // don't need to take any action unless an error is marked as fatal.
                c.OnError += (_, e) => consuming = !e.IsFatal;

                while (consuming) {
                    try {
                        var cr = c.Consume();
                        Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
                    } catch (ConsumeException e) {
                        Console.WriteLine($"Error occured: {e.Error.Reason}");
                    }
                }

                // Ensure the consumer leaves the group cleanly and final offsets are committed.
                c.Close();
            }
        }
    }
}
